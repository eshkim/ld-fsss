// Author: Murugeswari

#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <numeric>
#include <iomanip>
#include <sstream>
#include <vector>
#include <map>

#include "utils/math_utils.h"
#include "search_engine.h"
#include "states.h"
#include "prost_planner.h"

extern SearchEngine* rddllib; // defined in main.cc

class MyException : public std::exception {
public:
    std::string msg;
    MyException(std::string s) : msg(s) {}
    ~MyException() noexcept {}
    const char* what() const noexcept {
        return msg.c_str();
    }
};

// Returns PDState as std::string
std::string stateString(const PDState& state) {
    std::stringstream ss;
    state.printCompact(ss);
    std::string s = ss.str();
    s.pop_back();
    return s;
}

// Get integer value of action assigned by Prost
int encodeActionForProst(int index) {
    std::stringstream ss;
    for (int i=1; i<=SearchEngine::numActionFluents; i++) {
        std::string s = "0,";
        if (i == index) s = "1,";
        ss << s;
    }
    std::string action_str = ss.str(); // it is ok to have a comma at the end
    if (SearchEngine::actionBinaryToInt.count(action_str) == 0) { // std::map intialized in parser.cc
        throw MyException("In encodeActionForProst(int) (SearchEngine::actionBinaryToInt.count(action_str) == 0)");
    }
    return SearchEngine::actionBinaryToInt.at(action_str);
}

// Take action at curr state and return next state and reward; action is the integer value of action not an index
double takeAction(const PDState& curr, int action, PDState& next) {
    double rwd;
    rddllib->calcReward(curr, action, rwd);
    rddllib->calcSuccessorState(curr, action, next);
    for (unsigned int i=0; i<State::numberOfProbabilisticStateFluents; i++) {
        next.sample(i);
    }
    return rwd;
}

// Another version that returns the probability of the next state too
double takeAction2(const PDState& curr, int action, PDState& next, double& prob) {
    double rwd;
    rddllib->calcReward(curr, action, rwd);
    rddllib->calcSuccessorState(curr, action, next);
    prob =  1.0;
    for (unsigned int i=0; i<State::numberOfProbabilisticStateFluents; i++) {
        std::pair<double, double> outcome = next.sample(i);
        prob *= outcome.second;
    }
    return rwd;
}

// Just to avoid naming conflicts
namespace FS {
    int uniqueStateId;
    double Rmin;
    double Rmax;
    void(*DNN)(double*, double*); // pointer to the callback function that runs the DNN from Python
    int H; // horizon
    int D; // depth up to which discrepancies are allowed
    int K; // maximum number of discrepancies along a path
    int C; // number of successors
    int W; // number of actions at internal nodes
    int W0; // number of actions at the root
    double G; // discount parameter gamma
    int L; // initialize leaf nodes with the value function of the base policy
    int RH; // rollout horizon to set the values of leaf nodes if L > 0
    int RT; // number of rollout trajectories  to set the values of leaf nodes if L > 0
    std::map<std::string, int> stateIdMap;
    std::map<int, std::vector<double> > nnPolicyMap; // key is state id; vector of probabilities
    std::map<int, std::vector<int> > nnSortedActionMap; // key is state id; vector of action rank; NN action gets rank 0
    std::map<int, std::map<int, double> > stateActionRewardMap; // R(s,a) can be cached; key is state id and inner key is action
    std::map<int, double> nnStateValue; // key is state id; state value from NN

    // Assign (if needed) and return a unique state id
    inline int getStateId(const PDState& state) {
        std::string s = stateString(state);
        if (FS::stateIdMap.count(s) == 0) {
            FS::stateIdMap[s] = FS::uniqueStateId++;
        }
        return FS::stateIdMap.at(s);
    }

    inline void reset() {
        FS::uniqueStateId = 0;
        FS::stateIdMap.clear();
        FS::nnPolicyMap.clear();
        FS::nnSortedActionMap.clear();
        for (std::map<int, std::map<int, double> >::iterator it=FS::stateActionRewardMap.begin(); it!=FS::stateActionRewardMap.end(); it++) {
            (it->second).clear();
        }
        FS::stateActionRewardMap.clear();
        FS::nnStateValue.clear();
    }

    // Get action probability vector from NN
    inline void evaluateNN(const PDState& state, int id) { // id is the unique state id
        double* p = new double[SearchEngine::numberOfActions];
        double* s = new double[State::numberOfDeterministicStateFluents + State::numberOfProbabilisticStateFluents];
        for (int i=0; i<State::numberOfDeterministicStateFluents; i++) {
            s[i] = state.deterministicStateFluent(i);
        }
        for (int i=0; i<State::numberOfProbabilisticStateFluents; i++) {
            s[i+State::numberOfDeterministicStateFluents] = state.probabilisticStateFluent(i);
        }
        FS::DNN(s, p);
        std::vector<double> pi(p, p+SearchEngine::numberOfActions);
        FS::nnPolicyMap[id] = pi;
        std::vector<int> npi(pi.size());
        int n = 0;
        std::iota(npi.begin(), npi.end(), n++);
        std::sort(npi.begin(), npi.end(), [&](int j, int k) { return pi[j] >= pi[k]; });
        FS::nnSortedActionMap[id] = npi;
        delete[] p;
        delete[] s;
    }

    // Get the (n+1)^th best NN action for the state
    inline int nnAction(int n, const PDState& state) {
        int id = FS::getStateId(state);
        if (FS::nnSortedActionMap.count(id) == 0) {
            FS::evaluateNN(state, id);
        }
        return FS::nnSortedActionMap.at(id)[n];
    }

    // [V_min, V_max] for internal nodes
    inline std::pair<double, double> getDefaultBounds(int h) { // steps-to-go
        double g = (1.0 - pow(FS::G, (FS::L==0)?h:h-1+FS::RH)) / (1.0 - FS::G);
        return std::make_pair(FS::Rmin*g, FS::Rmax*g);
    }

    // Get value of nodes at depth H-1 of the FSSS DAG
    inline double nnActionReward(const PDState& curr, int action) {
        PDState next;
        double rwd = takeAction(curr, encodeActionForProst(action), next);
        FS::stateActionRewardMap[FS::getStateId(curr)][action] = rwd;
        return rwd;
    }


    inline std::pair<double, double> getLeafValue(const PDState& state) {
        double v = 0.0;
        switch (FS::L) {
            case 0: v = FS::nnActionReward(state, FS::nnAction(0, state));
                    break;
            case 1:
                PDState next, curr;
                for (int t=0; t<FS::RT; t++) {
                    v += takeAction(state, encodeActionForProst(FS::nnAction(0, state)), curr);
                    for (int h=0; h<FS::RH-1; h++) {
                        v += pow(FS::G, h+1) * takeAction(curr, encodeActionForProst(FS::nnAction(0, curr)), next);
                        curr = next;
                    }
                }
                v /= FS::RT;
        }
        return std::make_pair(v, v);
    }

    // Get saved state-action reward; index is the action index
    inline double getStateActionReward(const PDState& state, int action) {
        int id = FS::getStateId(state);
        if (FS::stateActionRewardMap.size() == 0) {
            throw MyException("In getStateActionReward(const PDState& state, int action) FS::stateActionRewardMap.size() == 0");
        }
        else if (FS::stateActionRewardMap.count(id) == 0) {
            throw MyException("In getStateActionReward(const PDState& state, int action) FS::stateActionRewardMap.count(id) == 0");
        }
        else if (FS::stateActionRewardMap.at(id).count(action) == 0) {
            throw MyException("In getStateActionReward(const PDState& state, int action) (FS::stateActionRewardMap.at(id)).count(action) == 0");
        }
        return FS::stateActionRewardMap.at(id).at(action);
    }
}


class Node {
private:
    PDState state;
    int depth;
    int discrepanciesRemaining;
    std::pair<double, double> value;
    std::map<int, std::pair<double, double> > qvalues; // key of map is action
    std::map<int, std::map<int, std::pair<int, std::pair<Node*, double> > > > successors; // key of main map is action and inner map is state id. For each state, we record frequency, node and probability
    std::map<int, std::map<int, Node*> > predecessors; // key of main map is state and inner map is action

public:
    Node(const PDState& s, int d, int k) : depth(d), discrepanciesRemaining(k) {
        state = PDState(s);
        if ((FS::H - depth) == 1) qvalues[FS::nnAction(0, state)] = value = FS::getLeafValue(state);
        else qvalues[FS::nnAction(0, state)] = value = FS::getDefaultBounds(FS::H - depth);
    }

    const PDState& getState() const {
        return state;
    }

    int getDepth() const {
        return depth;
    }

    int getDiscrepanciesRemaining() const {
        return discrepanciesRemaining;
    }

    // Stochastic branching factor of actions at this node
    int getStochasticBranchingFactor() const {
        if (depth == 0) return 10;
        else return FS::C;
    }

    // Check if the node is solved
    bool isSolved() const {
        if (MathUtils::doubleIsEqual(value.first, value.second)) return true;
        else return false;
    }

    // Get successors count for an action
    int successorCount(int action) const {
        int count = 0;
        if (successors.count(action) == 1) {
            for (std::map<int, std::pair<int, std::pair<Node*, double> > >::const_iterator it=(successors.at(action)).begin(); it!=(successors.at(action)).end(); it++) {
                count += (it->second).first;
            }
        }
        return count;
    }

    // Record new successor or update count for existing successor
    void addSuccessor(Node* z, int action, double p) {
        int id = FS::getStateId(z->getState());
        if ((successors.count(action) == 0) || ((successors.at(action)).count(id) == 0)) {
            successors[action][id] = std::make_pair(1, std::make_pair(z, p));
        }
        else {
            (successors[action][id]).first = ((successors.at(action)).at(id)).first + 1;
        }
    }

    // Record predecessor if not done already
    void addPredecessor(Node* x, int action) {
        int id = FS::getStateId(x->getState());
        if ((predecessors.count(id) == 0) || ((predecessors.at(id)).count(action) == 0)) {
            predecessors[id][action] = x;
        }
    }

    // Initialize qvalues for discrepancies
    void introduceDiscrepancies() {
        if ((depth <= FS::D) && (discrepanciesRemaining > 0)) {
            int k = 0;
            if (depth == 0) k = FS::W0;
            else k = FS::W;
            if (qvalues.size() < k) {
                int stepsToGo = FS::H - depth;
                std::pair<double, double> bound = FS::getDefaultBounds(stepsToGo);
                for (int j=1; j<k; j++) {
                    int action = FS::nnAction(j, state); // Action for j = 0 is already there
                    qvalues[action] = bound;
                }
            }
        }
    }

    // Get Max lower bound action
    int maxLowerBoundAction() const {
        if ((qvalues.size() == 0) || (qvalues.count(FS::nnAction(0, state)) == 0)) {
            throw MyException("In maxLowerBoundAction()");
        }

        int action = FS::nnAction(0, state);
        double qval = qvalues.at(action).first;
        double maxq = qval;
        int best_action = action;
        for (std::map<int, std::pair<double, double> >::const_iterator it=qvalues.begin(); it!=qvalues.end(); it++) {
            double q = (it->second).first;
            if (q > maxq) {
                maxq = q;
                best_action = it->first;
            }
        }

        std::cout << "Action: " << best_action << std::endl << std::endl;

        return best_action;
    }

    // Get Max upper bound action
    int maxUpperBoundAction() const {
        if (qvalues.size() == 0) {
            throw MyException("In maxUpperBoundAction(int&) qvalues.size() == 0");
        }
        int action = (qvalues.begin())->first;
        double maxuq = ((qvalues.begin())->second).second;
        for (std::map<int, std::pair<double, double> >::const_iterator it=qvalues.begin(); it!=qvalues.end(); it++) {
            double q = (it->second).second;
            if (q > maxuq) {
                maxuq = q;
                action = it->first;
            }
        }
        return action;
    }

    // Get Widest Interval Successor for an action
    Node* widestIntervalSuccessor(int action) const {
        if (successors.count(action) == 0) {
            throw MyException("In widestIntervalSuccessor(int) successors.count(action) == 0");
        }
        Node* z = (((successors.at(action)).begin())->second).second.first;
        double maxWidth = (z->value).second - (z->value).first;
        for (std::map<int, std::pair<int, std::pair<Node*, double> > >::const_iterator it=(successors.at(action)).begin(); it!=(successors.at(action)).end(); it++) {
            Node* w = (it->second).second.first;
            double width = (w->value).second - (w->value).first;
            if (width > maxWidth) {
                maxWidth = width;
                z = w;
            }
        }
        return z;
    }


    void updateBounds(int action) {
        if (successorCount(action) < getStochasticBranchingFactor()) {
            throw MyException("In updateBounds(int) successorCount(action) < getStochasticBranchingFactor()");
        }
        double a = 0.0;
        double b = 0.0;
        double s = 0.0;

        for (std::map<int, std::pair<int, std::pair<Node*, double> > >::iterator it=(successors.at(action)).begin(); it!=(successors.at(action)).end(); it++) {
            double p = (it->second).second.second;
            s += p;
        }

        for (std::map<int, std::pair<int, std::pair<Node*, double> > >::iterator it=(successors.at(action)).begin(); it!=(successors.at(action)).end(); it++) {
            Node* z = (it->second).second.first;
            double pp = (it->second).second.second / s;

            a += (((z->value).first) * pp);
            b += (((z->value).second) * pp);
        }
        a *= FS::G;
        b *= FS::G;

        double rwd = FS::getStateActionReward(state, action);
        qvalues[action] = std::make_pair(a+rwd, b+rwd);
        std::pair<double, double> v = qvalues.at(action);
        for (std::map<int, std::pair<double, double> >::iterator it=qvalues.begin(); it!=qvalues.end(); it++) {
            if ((it->second).first > v.first) {
                v.first = (it->second).first;
            }
            if ((it->second).second > v.second) {
                v.second = (it->second).second;
            }
        }
        bool bkwdpass = false;
        if ((v.first != value.first) || (v.second != value.second)) {
            bkwdpass = true;
        }
        value = v;
        if (bkwdpass) {
            backwardPass();
        }
    }

    void backwardPass() {
        if (predecessors.size() > 0) {
            for (std::map<int, std::map<int, Node*> >::iterator it=predecessors.begin(); it!=predecessors.end(); it++) {
                for (std::map<int, Node*>::iterator it2=(it->second).begin(); it2!=(it->second).end(); it2++) {
                    (it2->second)->updateBounds(it2->first);
                }
            }
        }
    }

    void disp2() const {
        for (std::map<int, std::map<int, std::pair<int, std::pair<Node*, double> > > >::const_iterator it=successors.begin(); it!=successors.end(); it++) {
            std::cout << "\nAction:      " << it->first << std::endl;
            std::cout << "Successors:";
            int count = 0;
            for (std::map<int, std::pair<int, std::pair<Node*, double> > >::const_iterator it2=it->second.begin(); it2!=it->second.end(); it2++) {
                std::cout << std::setw(3) << it2->first << "(" << std::setw(1) << it2->second.first << ")   ";
                std::cout << std::fixed << std::setprecision(2) << std::setw(5) << it2->second.second.second << "  ";
                if (it2->first != FS::getStateId((it2->second.second.first)->state)) std::cout << "Error!!!" << std::endl;
                count += it2->second.first;
            }
            std::cout << "\nTotal:       " << count << std::endl << std::endl;
        }
    }

    // Display node information
    void disp() const {
        if (!isSolved()) {
            std::cout << "Node not solved" << std::endl;
        }
        std::cout << "Value: ["
            << std::fixed << std::setprecision(2) << std::setw(5) << value.first << ", "
            << std::fixed << std::setprecision(2) << std::setw(5) << value.second << "]" << std::endl;
        for (std::map<int, std::pair<double, double> >::const_iterator it=qvalues.begin(); it!=qvalues.end(); it++) {
            std::cout << "Action: " << std::setw(2) << it->first << "   QValue: ["
                << std::fixed << std::setprecision(2) << std::setw(5) << (it->second).first << ", "
                << std::fixed << std::setprecision(2) << std::setw(5) << (it->second).second << "]" << std::endl;
        }

        // More display
        disp2();
    }


    // Destructor -  Clear all maps
    ~Node() {
        qvalues.clear();
        for (std::map<int, std::map<int, std::pair<int, std::pair<Node*, double> > > >::iterator it=successors.begin(); it!=successors.end(); it++) {
            (it->second).clear();
        }
        successors.clear();
        for (std::map<int, std::map<int, Node*> >::iterator it=predecessors.begin(); it!=predecessors.end(); it++) {
            (it->second).clear();
        }
        predecessors.clear();
    }
};


class FSSS {
private:
    std::vector<std::map<int, std::map<int, Node*> > > searchGraph; // key of first map is state id and inner map is discrepancies-remaining
    int trajectories;

    // Get graph size for given depth
    int getGraphSize(int depth) const {
        int nodes = 0;
        if (searchGraph[depth].size() > 0) {
            for (std::map<int, std::map<int, Node*> >::const_iterator it=searchGraph[depth].begin(); it!=searchGraph[depth].end(); it++) {
                nodes += (it->second).size();
            }
        }
        return nodes;
    }

    // Check if node exists in graph
    Node* getNodeFromGraph(int depth, const PDState& state, int dr) const {
        int id = FS::getStateId(state);
        if ((searchGraph[depth].size() > 0) && (searchGraph[depth].count(id) == 1) && (searchGraph[depth].at(id).count(dr) == 1)) {
            return searchGraph[depth].at(id).at(dr);
        }
        else return NULL;
    }

    // Add node to graph
    void addNodeToGraph(int depth, Node* x) {
        int dr = x->getDiscrepanciesRemaining();
        int id = FS::getStateId(x->getState());
        if ((searchGraph[depth].size() == 0) || (searchGraph[depth].count(id) == 0) || (searchGraph[depth].at(id).count(dr) == 0)) {
            searchGraph[depth][id][dr] = x;
        }
    }


public:
    // Constructor
    FSSS(int s) : trajectories(s) {
        searchGraph = std::vector<std::map<int, std::map<int, Node*> > >(FS::H);
    }

    // Compute graph Size
    int getGraphSize() const {
        int nodes = 0;
        for (int depth=0; depth<FS::H; depth++) {
            nodes += getGraphSize(depth);
        }
        return nodes;
    }

    // Clear the graph
    void clearGraph() {
        for (int depth=0; depth<FS::H; depth++) {
            if (searchGraph[depth].size() > 0) {
                for (std::map<int, std::map<int, Node*> >::iterator it=searchGraph[depth].begin(); it!=searchGraph[depth].end(); it++) {
                    for (std::map<int, Node*>::iterator it2=(it->second).begin(); it2!=(it->second).end(); it2++) {
                        delete (it2->second);
                    }
                    (it->second).clear();
                }
            }
            searchGraph[depth].clear();
        }
        FS::reset();
    }

    // Estimate QValues - the primary member function of FSSS
    void estimateQ(Node* x) {
        if (x->isSolved()) {
            return;
        }
        x->introduceDiscrepancies(); // if it was not done already
        int action = x->maxUpperBoundAction();
        PDState next;
        PDState curr = x->getState();
        int depth = x->getDepth() + 1;
        int k = 0;
        if (depth <= FS::D) {
            if (action == FS::nnAction(0, curr)) k = x->getDiscrepanciesRemaining();
            else k = x->getDiscrepanciesRemaining() - 1;
        }
        while (x->successorCount(action) < x->getStochasticBranchingFactor()) {
            double p;
            double rwd = takeAction2(curr, encodeActionForProst(action), next, p);

            FS::stateActionRewardMap[FS::getStateId(curr)][action] = rwd;
            Node* z = getNodeFromGraph(depth, next, k);
            if (!z) {
                Node* w = new Node(next, depth, k);
                addNodeToGraph(depth, w);
                z = w;
            }
            x->addSuccessor(z, action, p);
            z->addPredecessor(x, action);
        }
        Node* y = x->widestIntervalSuccessor(action);
        estimateQ(y);
        x->updateBounds(action);
    }

    // Build FSSS Search Graph
    Node* buildSearchGraph(const PDState& state, int& sim) {
        Node* root = new Node(state, 0, FS::K); // middle parameter is depth
        addNodeToGraph(0, root); // parameter 1 is depth
        while (!(root->isSolved())) {
            sim++;
            estimateQ(root);
            if (sim > trajectories) break;
        }
        return root;
    }

    // Destructor
    ~FSSS() {
        clearGraph();
    }
};


class LDS {
private:
    FSSS* alg;

public:
    // Constructor
    LDS(void(*fp)(double*, double*), int horizon, int depth, int children, double gamma, int trajectories, int discrepancies, int actions, int rootactions, int initleafnodes, int rolloutHorizon, int rolloutTrajectories) {
        FS::uniqueStateId = 0;
        FS::DNN = fp;
        FS::H = horizon;
        FS::D = depth;
        FS::C = children;
        FS::G = gamma;
        FS::K = discrepancies;
        FS::W = actions;
        FS::W0 = rootactions;
        FS::L = initleafnodes;
        FS::RH = rolloutHorizon;
        FS::RT = rolloutTrajectories;
        alg = new FSSS(trajectories);
    }

    // Destructor
    ~LDS() {
        delete alg;
    }

    // Initialize reward range
    void initRewardRange() {
        FS::Rmin = (rddllib->rewardCPF)->getMinVal();
        FS::Rmax = (rddllib->rewardCPF)->getMaxVal();
        if (SearchEngine::numberOfActions < FS::W) FS::W = SearchEngine::numberOfActions;
        if (SearchEngine::numberOfActions < FS::W0) FS::W0 = SearchEngine::numberOfActions;
    }

    // Run Limited Discrepancy Search
    int runSearch(const PDState& curr) {
        int action = FS::nnAction(0, curr); // set default action to the NN action
        try {
            int sim = 0; // simulations (trajectories)
            Node* root = alg->buildSearchGraph(curr, sim);
            if (!(root->isSolved())) {
                std::cout << "Simulations: " << sim << std::endl;
            }

            action = root->maxLowerBoundAction();
            alg->clearGraph();
        }
        catch (std::exception& e) {
            std::cout << e.what() << std::endl;
        }
        return encodeActionForProst(action);
    }

    // Return NN action
    int evaluate(const PDState& curr) {
        int action = FS::nnAction(0, curr);
        return encodeActionForProst(action);
    }
};
