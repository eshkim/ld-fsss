#ifndef RDDL_CLIENT_H
#define RDDL_CLIENT_H

#include <map>
#include <memory>
#include <string>
#include <vector>

// Murugeswari - Changes begin
#include "states.h"
#include "rddl_instance.h"
// Murugeswari - Changes end

class ProstPlanner;
class XMLNode;

class IPPCClient {
public:
    IPPCClient(std::string _hostName, unsigned short _port);
    ~IPPCClient();

    // Murugeswari - Changes begin
    //void run(std::string const& problem, std::string& plannerDesc);
    void run(std::string const&, std::string&, struct ProblemSpec*, void(*fpDsInit)(), int(*fpDsRun)(const PDState&));
    void run(std::string const&, std::string&, struct ProblemSpec*, void(*fpinit)(int, int), int(*fprun)(const PDState&, int, int), int, int);

    int getNumRounds() {
        return numberOfRounds;
    }
    // Murugeswari - Changes end

    std::unique_ptr<ProstPlanner>& getPlanner() {
        return planner;
    }

private:
    void initConnection();
    int connectToServer();
    void closeConnection();

    void initSession(std::string const& rddlProblem, std::string& plannerDesc);
    void finishSession();

    void initRound(std::vector<double>& initialState, double& immediateReward);
    void finishRound(XMLNode const* node, double& immediateReward);

    bool submitAction(std::vector<std::string>& action,
                      std::vector<double>& nextState, double& immediateReward);

    void readState(XMLNode const* node, std::vector<double>& nextState,
                   double& immediateReward);
    void readVariable(XMLNode const* node,
                      std::map<std::string, std::string>& result);

    // If the client call did not contain a task file, we have to read the task
    // description from the server and run the external parser to create a task
    // in PROST format.
    void executeParser(std::string const& problemName,
                       std::string const& taskDesc);

    // Murugeswari - Changes begin
    void parseRDDLFile(std::string const& problemName, std::string& plannerDesc);
    // Murugeswari - Changes end

    std::unique_ptr<ProstPlanner> planner;
    std::string hostName;
    unsigned short port;
    int socket;

    int numberOfRounds;

    long remainingTime;

    std::map<std::string, int> stateVariableIndices;
    std::vector<std::vector<std::string>> stateVariableValues;
};

#endif
