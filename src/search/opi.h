// Author: Murugeswari

#include <iostream>
#include <vector>
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <map>
#include <cmath>

#include "states.h"

extern SearchEngine* rddllib; // defined in main.cc

class ValueFn {
private:
    void(*NNPolicy)(double*, double*); // base policy
    int num_det_state_vars;
    int num_prob_state_vars;
    int num_succ;
    int num_succ_pi;
    int fflag;
    double cparam;
    double gamma;

    int getNNPolicyAction(const PDState&, int) const; // state, rank (0 is the policy action)

public:
    ValueFn() {}
    ValueFn(void(*fp)(double*, double*), int, int, int, double, double);
    void init(int, int); //  number of deterministic and probabilistic state variables
    std::pair<int, double> estimateStateValueFinal(const PDState&, int, int) const; // state, #actions, flag for QValue computation
    std::pair<int, double> estimateStateValueFinalTime(const PDState&, int, int) const; // state, #actions, flag for QValue computation
    std::pair<int, double> estimateStateValueEquivalentMCRollout(const PDState&, int, int) const; // state, #actions, flag for QValue computation
    std::pair<int, double> estimateStateValueEquivalentPMCRollout(const PDState&, int, int) const; // state, #actions, flag for QValue computation
    ~ValueFn() {}
};


ValueFn::ValueFn(void(*fp)(double*, double*), int num_root_succ, int nsucc, int fval, double cval, double g) {
    NNPolicy = fp;
    num_succ = num_root_succ;
    rddllib = NULL;
    num_succ_pi = nsucc;
    fflag = fval;
    cparam = cval;
    gamma = g;
}


void ValueFn::init(int ndet, int nprob) {
    num_det_state_vars = ndet;
    num_prob_state_vars = nprob;
}

int ValueFn::getNNPolicyAction(const PDState& state, int index) const {
    if (!rddllib) throw MyException("The function can be invoked only after rddllib is initialized in ippc_client.cc");
    double* p = new double[SearchEngine::numberOfActions];
    double* s = new double[num_det_state_vars + num_prob_state_vars];
    for (int i=0; i<num_det_state_vars; i++) {
        s[i] = state.deterministicStateFluent(i);
    }
    for (int i=0; i<num_prob_state_vars; i++) {
        s[num_det_state_vars+i] = state.probabilisticStateFluent(i);
    }
    NNPolicy(s, p); // can save the policy for the state
    std::vector<double> pi(p, p+SearchEngine::numberOfActions);
    std::vector<int> npi(pi.size());
    int n = 0;
    std::iota(npi.begin(), npi.end(), n++);
    std::sort(npi.begin(), npi.end(), [&](int j, int k) { return pi[j] >= pi[k]; });
    delete[] p;
    delete[] s;

    // convert to the action index used in PROST
    std::stringstream ss;
    for (int i=1; i<=SearchEngine::numActionFluents; i++) {
        std::string s = "0,";
        if (i == npi[index]) s = "1,";
        ss << s;
    }
    std::string action_str = ss.str(); // it is ok to have a comma at the end
    if (SearchEngine::actionBinaryToInt.count(action_str) == 0) { // std::map intialized in parser.cc
        throw MyException("In encodeActionForProst(int) (SearchEngine::actionBinaryToInt.count(action_str) == 0)");
    }
    return SearchEngine::actionBinaryToInt.at(action_str);
}


// MC Rollout
std::pair<int, double> ValueFn::estimateStateValueEquivalentMCRollout(const PDState& state, int k, int h) const {
    int horizon = state.stepsToGo()-1;
    if (h < horizon) horizon = h;

    std::pair<int, double> gsrch = estimateStateValueFinalTime(state, k, h);

    std::vector<int> actions(k);
    std::vector<double> q(k, 0.0);

    // Save reward and state-variable probabilities of all actions at the root
    for (int a=0; a<k; a++) {
        actions[a] = getNNPolicyAction(state, a);
    }

    // MC Rollout
    Stopwatch sw;
    double tm = sw();
    int tnum = 1;
    while (1) {
        PDState next;
        double rwd;
        for (int a=0; a<k; a++) {
            PDState curr = state;
            int action = actions[a];
            double tsum = 0.0;
            rddllib->calcReward(curr, action, rwd);
            rddllib->calcSuccessorState(curr, action, next);
            for (int i=0; i<num_prob_state_vars; i++) {
                next.sample(i);
            }
            tsum += rwd;
            for (int i=0; i<horizon; i++) {
                curr = next;
                action = getNNPolicyAction(curr, 0);
                rddllib->calcReward(curr, action, rwd);
                rddllib->calcSuccessorState(curr, action, next);
                for (int i=0; i<num_prob_state_vars; i++) {
                    next.sample(i);
                }
                tsum += rwd;
            }
            q[a] += (tsum - q[a]) / tnum;
        }
        tm = sw();
        double timeleft = gsrch.second - tm;
        double timereq = tm / tnum;
        if ((horizon == 0) || (timeleft < (0.5 * timereq))) break;
        tnum++;
    }

    for (int a=0; a<k; a++) {
        double alpha = cparam;
        if (q[a] < 0) alpha *= -1;
        double qnew = q[a];
        if (a == 0) qnew *= (1 + alpha);
        else qnew *= (1 - alpha);
        q[a] = qnew;
    }

    double qv = q[0];
    int best = actions[0];
    for (int a=1; a<k; a++) {
        if (q[a] > qv) {
            qv = q[a]; best = actions[a];
        }
    }

    if (best != actions[0]) {
        std::cout << "Different action" << std::endl;
    }

    // clean up
    actions.clear();
    q.clear();

    return std::make_pair(best, qv);
}


// MC Rollout
std::pair<int, double> ValueFn::estimateStateValueEquivalentPMCRollout(const PDState& state, int k, int h) const {
    int horizon = state.stepsToGo()-1;
    if (h < horizon) horizon = h;

    std::pair<int, double> gsrch = estimateStateValueFinalTime(state, k, h);

    int num_state_vars = num_det_state_vars + num_prob_state_vars;
    std::vector<int> actions(k);
    std::vector<double> q(k, 0.0);

    std::vector<std::map<std::string, int> > rsucc(k);
    std::vector<std::vector<double> > rootSuccProb(k, std::vector<double>(num_state_vars));

    // Save reward and state-variable probabilities of all actions at the root
    for (int a=0; a<k; a++) {
        PDState curr = state;
        int action = getNNPolicyAction(curr, a);
        actions[a] = action;

        PDState next;
        double rwd;
        rddllib->calcReward(curr, action, rwd);
        rddllib->calcSuccessorState(curr, action, next);

        for (int i=0; i<num_det_state_vars; i++) {
            rootSuccProb[a][i] = next.deterministicStateFluent(i);
        }

        for (int i=0; i<num_prob_state_vars; i++) {
            int j = num_det_state_vars + i;
            std::pair<double, double> outcome = next.sample(i);
            if ((int) outcome.first == 1) rootSuccProb[a][j] = outcome.second;
            else rootSuccProb[a][j] = 1 - outcome.second;
        }
    }

    // MC Rollout
    Stopwatch sw;
    double tm = sw();
    int tnum = 1;
    while (1) {
        PDState next;
        double rwd;
        for (int a=0; a<k; a++) {
            PDState curr = state;
            int action = actions[a];
            double tsum = 0.0;
            rddllib->calcReward(curr, action, rwd);
            rddllib->calcSuccessorState(curr, action, next);
            tsum += rwd;

            std::string stt;
            std::stringstream ss;
            for (int i=0; i<num_det_state_vars; i++) {
                int dvar = (int) next.deterministicStateFluent(i);
                if (dvar == 0) ss << "0";
                else ss << "1";
                if (dvar != (int) rootSuccProb[a][i]) throw MyException("Deterministic state variable values do not match");
            }
            for (int i=num_det_state_vars; i<num_state_vars; i++) {
                int j = i - num_det_state_vars;
                int pvar = (int) next.sample(j).first;
                if (pvar == 1) ss << "1";
                else ss << "0";
            }
            stt = ss.str();
            if (rsucc[a].count(stt) == 0) rsucc[a][stt] = 1;
            else rsucc[a][stt] = rsucc[a].at(stt) + 1;

            for (int i=0; i<horizon; i++) {
                curr = next;
                action = getNNPolicyAction(curr, 0);
                rddllib->calcReward(curr, action, rwd);
                rddllib->calcSuccessorState(curr, action, next);
                for (int i=0; i<num_prob_state_vars; i++) {
                    next.sample(i);
                }
                tsum += rwd;
            }
            q[a] += (tsum - q[a]) / tnum;
        }
        tm = sw();
        double timeleft = gsrch.second - tm;
        double timereq = tm / tnum;
        if ((horizon == 0) || (timeleft < (0.5 * timereq))) break;
        tnum++;
    }

    std::vector<double> psum(k, 0.0);
    for (int a=0; a<k; a++) {
        for (std::map<std::string, int>::iterator it=rsucc[a].begin(); it!=rsucc[a].end(); it++) {
            std::string w = it->first;
            double p = 1.0;
            for (int i=0; i<num_state_vars; i++) {
                int z = w[i] - '0';
                if (z == 1) p *= rootSuccProb[a][i];
                else p *= 1 - rootSuccProb[a][i];
            }
            psum[a] += p;
        }
    }

    for (int a=0; a<k; a++) {
        double alpha = 0.0;
        switch (fflag) {
            case 3:
                alpha = cparam;
                break;
            default:
                alpha = cparam * (1 - psum[a]);
                break;
        }
        if (q[a] < 0) alpha *= -1;
        double qnew = q[a];
        if (a == 0) qnew *= (1 + alpha);
        else qnew *= (1 - alpha);
        q[a] = qnew;
    }

    double qv = q[0];
    int best = actions[0];
    for (int a=1; a<k; a++) {
        if (q[a] > qv) {
            qv = q[a]; best = actions[a];
        }
    }

    if (best != actions[0]) {
        std::cout << "Different action" << std::endl;
    }

    // clean up
    for (int i=0; i<k; i++) {
        rootSuccProb[i].clear();
    }
    rootSuccProb.clear();

    for (int i=0; i<k; i++) {
        rsucc[i].clear();
    }
    rsucc.clear();

    actions.clear();
    q.clear();

    return std::make_pair(best, qv);
}


// Replace int keys with string keys in maps
std::pair<int, double> ValueFn::estimateStateValueFinal(const PDState& state, int k, int h) const {
    int horizon = state.stepsToGo()-1;
    if (h < horizon) horizon = h;

    int num_state_vars = num_det_state_vars + num_prob_state_vars;
    std::vector<std::map<std::string, std::pair<double, std::vector<double> > > > svhat(horizon+1);
    std::vector<int> actions(k);
    std::vector<double> q(k);
    std::vector<std::vector<double> > rootSuccProb(k, std::vector<double>(num_state_vars));

    // Save reward and state-variable probabilities of all actions at the root
    for (int a=0; a<k; a++) {
        PDState curr = state;
        int action = getNNPolicyAction(curr, a);
        actions[a] = action;

        PDState next;
        double rwd;
        rddllib->calcReward(curr, action, rwd);
        rddllib->calcSuccessorState(curr, action, next);

        q[a] = rwd;

        for (int i=0; i<num_det_state_vars; i++) {
            rootSuccProb[a][i] = next.deterministicStateFluent(i);
        }

        for (int i=0; i<num_prob_state_vars; i++) {
            int j = num_det_state_vars + i;
            std::pair<double, double> outcome = next.sample(i);
            if ((int) outcome.first == 1) rootSuccProb[a][j] = outcome.second;
            else rootSuccProb[a][j] = 1 - outcome.second;
        }
    }

    // Generate and store successors of the root state
    for (int a=0; a<k; a++) {
        PDState curr = state;
        int action = actions[a];

        PDState next;
        rddllib->calcSuccessorState(curr, action, next);

        std::string dstr;
        std::stringstream dss;
        for (int i=0; i<num_det_state_vars; i++) {
            int dvar = (int) next.deterministicStateFluent(i);
            if (dvar == 0) dss << "0";
            else dss << "1";
            if (dvar != (int) rootSuccProb[a][i]) throw MyException("Deterministic state variable values do not match");
        }
        dstr = dss.str();

        for (int c=0; c<num_succ; c++) {
            std::string stt;
            std::stringstream ss;
            ss << dstr;

            for (int i=num_det_state_vars; i<num_state_vars; i++) {
                int j = i - num_det_state_vars;
                int pvar = (int) next.sample(j).first;
                if (pvar == 1) ss << "1";
                else ss << "0";
            }
            stt = ss.str();

            if (svhat[0].count(stt) == 0) {
                svhat[0][stt] = std::make_pair(0.0, std::vector<double>(num_state_vars, 0.0));
            }
        }
    }

    // Generate states for depths 1 to horizon for the base policy
    for (int d=0; d<horizon; d++) {
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[d].begin(); it!=svhat[d].end(); it++) {
            std::vector<double> z(num_state_vars, 0.0);
            std::string w = it->first;

            if (w.length() != num_state_vars) throw MyException("w.length() != num_state_vars");

            for (int i=0; i<num_state_vars; i++) {
                z[i] = w[i]- '0';
            }

            PDState curr = State(z, horizon-d);
            int action = getNNPolicyAction(curr, 0);

            PDState next;
            double rwd;
            rddllib->calcReward(curr, action, rwd);
            rddllib->calcSuccessorState(curr, action, next);

            svhat[d][it->first].first = rwd;

            for (int i=0; i<num_det_state_vars; i++) {
                (svhat[d][it->first].second)[i] = next.deterministicStateFluent(i);
            }

            for (int i=0; i<num_prob_state_vars; i++) {
                int j = num_det_state_vars + i;
                std::pair<double, double> outcome = next.sample(i);
                if ((int) outcome.first == 1) (svhat[d][it->first].second)[j] = outcome.second;
                else (svhat[d][it->first].second)[j] = 1 - outcome.second;
            }

            std::string dstr;
            std::stringstream dss;
            for (int i=0; i<num_det_state_vars; i++) {
                if ((int) next.deterministicStateFluent(i) == 0) dss << "0";
                else dss << "1";
            }
            dstr = dss.str();

            for (int c=0; c<num_succ_pi; c++) {
                std::string stt;
                std::stringstream ss;
                ss << dstr;

                for (int i=num_det_state_vars; i<num_state_vars; i++) {
                    int j = i - num_det_state_vars;
                    int pvar = (int) next.sample(j).first;
                    if (pvar == 1) ss << "1";
                    else ss << "0";
                }
                stt = ss.str();

                if (svhat[d+1].count(stt) == 0) {
                    svhat[d+1][stt] = std::make_pair(0.0, std::vector<double>(num_state_vars, 0.0));
                }
            }
        }
    }

    // compute the d steps-to-go values for depths 0 to horizon-2
    for (int d=horizon-2; d>=0; d--) {
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[d].begin(); it!=svhat[d].end(); it++) {
            double psum = 0.0;
            for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it2=svhat[d+1].begin(); it2!=svhat[d+1].end(); it2++) {
                std::string w = it2->first;
                double p = 1.0;
                for (int i=0; i<num_state_vars; i++) {
                    int z = w[i] - '0';
                    if (z == 1) p *= (it->second.second)[i];
                    else p *= 1 - (it->second.second)[i];
                }
                psum += p;
            }
            double npsum = 0.0;
            for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it2=svhat[d+1].begin(); it2!=svhat[d+1].end(); it2++) {
                std::string w = it2->first;
                double p = 1.0;
                for (int i=0; i<num_state_vars; i++) {
                    int z = w[i] - '0';
                    if (z == 1) p *= (it->second.second)[i];
                    else p *= 1 - (it->second.second)[i];
                }
                (it->second).first += (p / psum) * (it2->second).first;
                npsum += p / psum;
            }
            if (!(MathUtils::doubleIsEqual(npsum, 1.0))) {
                std::cout << "depth: " << d << " state: " << it->first << " psum: " << psum << " npsum: " << npsum << std::endl;
                throw MyException("Steps-to-go value computation: Normalized probabilities do not sum to 1");
            }
        }
    }

    std::vector<double> psum(k, 0.0);
    for (int a=0; a<k; a++) {
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[0].begin(); it!=svhat[0].end(); it++) {
            std::string w = it->first;
            double p = 1.0;
            for (int i=0; i<num_state_vars; i++) {
                int z = w[i] - '0';
                if (z == 1) p *= rootSuccProb[a][i];
                else p *= 1 - rootSuccProb[a][i];
            }
            psum[a] += p;
        }

        double npsum = 0.0;
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[0].begin(); it!=svhat[0].end(); it++) {
            std::string w = it->first;
            double p = 1.0;
            for (int i=0; i<num_state_vars; i++) {
                int z = w[i] - '0';
                if (z == 1) p *= rootSuccProb[a][i];
                else p *= 1 - rootSuccProb[a][i];
            }
            q[a] += (p / psum[a]) * (it->second).first;
            npsum += p / psum[a];
        }

        if (!(MathUtils::doubleIsEqual(npsum, 1.0))) {
            std::cout << "a: " << a << " Action: " << actions[a] << " psum: " << psum[a] << " npsum: " << npsum << std::endl;
            throw MyException("Qvalue computation: Normalized probabilities do not sum to 1");
        }
    }

    for (int a=0; a<k; a++) {
        double alpha = 0.0;
        switch (fflag) {
            case 3:
                alpha = cparam;
                break;
            default:
                alpha = cparam * (1 - psum[a]);
                break;
        }
        if (q[a] < 0) alpha *= -1;
        double qnew = q[a];
        if (a == 0) qnew *= (1 + alpha);
        else qnew *= (1 - alpha);
        q[a] = qnew;
    }

    double qv = q[0];
    int best = actions[0];
    for (int a=1; a<k; a++) {
        if (q[a] > qv) {
            qv = q[a]; best = actions[a];
        }
    }

    if (best != actions[0]) {
        std::cout << "Different action" << std::endl;
    }

    // clean up
    for (int d=0; d<horizon; d++) {
        svhat[d].clear();
    }
    svhat.clear();

    for (int i=0; i<k; i++) {
        rootSuccProb[i].clear();
    }
    rootSuccProb.clear();
    actions.clear();
    q.clear();

    return std::make_pair(best, qv);
}

// Replace int keys with string keys in maps
std::pair<int, double> ValueFn::estimateStateValueFinalTime(const PDState& state, int k, int h) const {
    // Dummy NN call to avoid the NN Initialization time in sw1 below
    getNNPolicyAction(state, 0);

    Stopwatch sw1;
    int horizon = state.stepsToGo()-1;
    if (h < horizon) horizon = h;

    int num_state_vars = num_det_state_vars + num_prob_state_vars;
    std::vector<std::map<std::string, std::pair<double, std::vector<double> > > > svhat(horizon+1);
    std::vector<int> actions(k);
    std::vector<double> q(k);
    std::vector<std::vector<double> > rootSuccProb(k, std::vector<double>(num_state_vars));

    // Save reward and state-variable probabilities of all actions at the root
    for (int a=0; a<k; a++) {
        PDState curr = state;
        int action = getNNPolicyAction(curr, a);
        actions[a] = action;

        PDState next;
        double rwd;
        rddllib->calcReward(curr, action, rwd);
        rddllib->calcSuccessorState(curr, action, next);

        q[a] = rwd;

        for (int i=0; i<num_det_state_vars; i++) {
            rootSuccProb[a][i] = next.deterministicStateFluent(i);
        }

        for (int i=0; i<num_prob_state_vars; i++) {
            int j = num_det_state_vars + i;
            std::pair<double, double> outcome = next.sample(i);
            if ((int) outcome.first == 1) rootSuccProb[a][j] = outcome.second;
            else rootSuccProb[a][j] = 1 - outcome.second;
        }
    }

    // Generate and store successors of the root state
    for (int a=0; a<k; a++) {
        PDState curr = state;
        int action = actions[a];

        PDState next;
        rddllib->calcSuccessorState(curr, action, next);

        std::string dstr;
        std::stringstream dss;
        for (int i=0; i<num_det_state_vars; i++) {
            int dvar = (int) next.deterministicStateFluent(i);
            if (dvar == 0) dss << "0";
            else dss << "1";
            if (dvar != (int) rootSuccProb[a][i]) throw MyException("Deterministic state variable values do not match");
        }
        dstr = dss.str();

        for (int c=0; c<num_succ; c++) {
            std::string stt;
            std::stringstream ss;
            ss << dstr;

            for (int i=num_det_state_vars; i<num_state_vars; i++) {
                int j = i - num_det_state_vars;
                int pvar = (int) next.sample(j).first;
                if (pvar == 1) ss << "1";
                else ss << "0";
            }
            stt = ss.str();

            if (svhat[0].count(stt) == 0) {
                svhat[0][stt] = std::make_pair(0.0, std::vector<double>(num_state_vars, 0.0));
            }
        }
    }

    // Generate states for depths 1 to horizon for the base policy
    for (int d=0; d<horizon; d++) {
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[d].begin(); it!=svhat[d].end(); it++) {
            std::vector<double> z(num_state_vars, 0.0);
            std::string w = it->first;

            if (w.length() != num_state_vars) throw MyException("w.length() != num_state_vars");

            for (int i=0; i<num_state_vars; i++) {
                z[i] = w[i]- '0';
            }

            PDState curr = State(z, horizon-d);
            int action = getNNPolicyAction(curr, 0);

            PDState next;
            double rwd;
            rddllib->calcReward(curr, action, rwd);
            rddllib->calcSuccessorState(curr, action, next);

            svhat[d][it->first].first = rwd;

            if (d < (horizon-1)) {
                for (int i=0; i<num_det_state_vars; i++) {
                    (svhat[d][it->first].second)[i] = next.deterministicStateFluent(i);
                }

                for (int i=0; i<num_prob_state_vars; i++) {
                    int j = num_det_state_vars + i;
                    std::pair<double, double> outcome = next.sample(i);
                    if ((int) outcome.first == 1) (svhat[d][it->first].second)[j] = outcome.second;
                    else (svhat[d][it->first].second)[j] = 1 - outcome.second;
                }

                std::string dstr;
                std::stringstream dss;
                for (int i=0; i<num_det_state_vars; i++) {
                    if ((int) next.deterministicStateFluent(i) == 0) dss << "0";
                    else dss << "1";
                }
                dstr = dss.str();

                for (int c=0; c<num_succ_pi; c++) {
                    std::string stt;
                    std::stringstream ss;
                    ss << dstr;

                    for (int i=num_det_state_vars; i<num_state_vars; i++) {
                        int j = i - num_det_state_vars;
                        int pvar = (int) next.sample(j).first;
                        if (pvar == 1) ss << "1";
                        else ss << "0";
                    }
                    stt = ss.str();

                    if (svhat[d+1].count(stt) == 0) {
                        svhat[d+1][stt] = std::make_pair(0.0, std::vector<double>(num_state_vars, 0.0));
                    }
                }
            }

        }
    }

    // compute the d steps-to-go values for depths 0 to horizon-2
    for (int d=horizon-2; d>=0; d--) {
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[d].begin(); it!=svhat[d].end(); it++) {
            double psum = 0.0;
            for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it2=svhat[d+1].begin(); it2!=svhat[d+1].end(); it2++) {
                std::string w = it2->first;
                double p = 1.0;
                for (int i=0; i<num_state_vars; i++) {
                    int z = w[i] - '0';
                    if (z == 1) p *= (it->second.second)[i];
                    else p *= 1 - (it->second.second)[i];
                }
                psum += p;
            }
            double npsum = 0.0;
            for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it2=svhat[d+1].begin(); it2!=svhat[d+1].end(); it2++) {
                std::string w = it2->first;
                double p = 1.0;
                for (int i=0; i<num_state_vars; i++) {
                    int z = w[i] - '0';
                    if (z == 1) p *= (it->second.second)[i];
                    else p *= 1 - (it->second.second)[i];
                }
                (it->second).first += (p / psum) * (it2->second).first;
                npsum += p / psum;
            }
            if (!(MathUtils::doubleIsEqual(npsum, 1.0))) {
                std::cout << "depth: " << d << " state: " << it->first << " psum: " << psum << " npsum: " << npsum << std::endl;
                throw MyException("Steps-to-go value computation: Normalized probabilities do not sum to 1");
            }
        }
    }

    std::vector<double> psum(k, 0.0);
    for (int a=0; a<k; a++) {
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[0].begin(); it!=svhat[0].end(); it++) {
            std::string w = it->first;
            double p = 1.0;
            for (int i=0; i<num_state_vars; i++) {
                int z = w[i] - '0';
                if (z == 1) p *= rootSuccProb[a][i];
                else p *= 1 - rootSuccProb[a][i];
            }
            psum[a] += p;
        }

        double npsum = 0.0;
        for (std::map<std::string, std::pair<double, std::vector<double> > >::iterator it=svhat[0].begin(); it!=svhat[0].end(); it++) {
            std::string w = it->first;
            double p = 1.0;
            for (int i=0; i<num_state_vars; i++) {
                int z = w[i] - '0';
                if (z == 1) p *= rootSuccProb[a][i];
                else p *= 1 - rootSuccProb[a][i];
            }
            q[a] += (p / psum[a]) * (it->second).first;
            npsum += p / psum[a];
        }

        if (!(MathUtils::doubleIsEqual(npsum, 1.0))) {
            std::cout << "a: " << a << " Action: " << actions[a] << " psum: " << psum[a] << " npsum: " << npsum << std::endl;
            throw MyException("Qvalue computation: Normalized probabilities do not sum to 1");
        }
    }
    double tm1 = sw1();

    for (int a=0; a<k; a++) {
        double alpha = 0.0;
        switch (fflag) {
            case 3:
                alpha = cparam;
                break;
            default:
                alpha = cparam * (1 - psum[a]);
                break;
        }
        if (q[a] < 0) alpha *= -1;
        double qnew = q[a];
        if (a == 0) qnew *= (1 + alpha);
        else qnew *= (1 - alpha);
        q[a] = qnew;
    }

    double qv = q[0];
    for (int a=1; a<k; a++) {
        if (q[a] > qv) {
            qv = q[a];
        }
    }

    // total nodes
    int nncalls = k;
    for (int d=0; d<horizon; d++) {
        nncalls += svhat[d].size();
    }

    // clean up
    for (int d=0; d<horizon; d++) {
        svhat[d].clear();
    }
    svhat.clear();

    for (int i=0; i<k; i++) {
        rootSuccProb[i].clear();
    }
    rootSuccProb.clear();
    actions.clear();
    q.clear();

    return std::make_pair(nncalls, tm1);
}
