#ifndef RDDL_INSTANCE_H
#define RDDL_INSTANCE_H

struct ProblemSpec {
    int num_state_vars;
    int num_action_vars;
    int num_actions;
    int horizon;
    double* initial_state;
};

#endif
