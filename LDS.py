# Author: Murugeswaari

import numpy.ctypeslib as npct
from Env import Env
import numpy as np
import argparse
import random
import sys
import pwd
import os
from pathlib import Path

if not sys.platform.startswith("linux"):
    print("Only Linux is supported")
    sys.exit()

parser = argparse.ArgumentParser()
group1 = parser.add_argument_group('group1', 'Mandatory Arguments')
group1.add_argument('--problem', help='RDDL Problem', default='game_of_life_inst_mdp__1')
group1.add_argument('--host', help='RDDLSim IP Address', default='10.0.2.2')
group1.add_argument('--port', help='RDDLSim Port', default=2323, type=int)
group1.add_argument('--dnn_ckpt_file', help='DNN Model Checkpoint File',
    default='./base_policies/good/game_of_life/JS35B1/game_of_life_inst_mdp__1.ckpt')
group1.add_argument('--rddllib', help='RDDL Dynamic Library Name', default='./clibxx.so')

group2 = parser.add_mutually_exclusive_group(required=True)
group2.add_argument('--evaluate_nn', help='Evaluate the DNN Policy', type=int)
group2.add_argument('--run_ldfsss', help='Run Limited Discrepancy Search', type=int)
group2.add_argument('--dag_rollout', help='Run DAG based Rollout', type=int)
group2.add_argument('--mc_rollout', help='Run Monte-Carlo Rollout', type=int)

group3 = parser.add_argument_group('group3', 'Arguments for Search')
# The following 5 arguments are for ld_fsss, dag_rollout and mc_rollout
group3.add_argument('--search_horizon', help='Search Horizon', default=3, type=int)
group3.add_argument('--num_root_succ', help='Number of Successor States of the root node', default=3, type=int)
group3.add_argument('--discount_factor', help='Discount Factor', default=0.9, type=float)
group3.add_argument('--num_succ', help='Number of successors at internal nodes', default=3, type=int)
group3.add_argument('--root_actions', help='Number of Actions at the Root Node', default=4, type=int)

# The following 7 arguments are only for ld_fsss only
group3.add_argument('--internal_node_actions', help='Number of Actions at Internal Nodes', default=1, type=int)
group3.add_argument('--discrepancy_depth', help='Depth until which Discrepancies are Allowed', default=0, type=int)
group3.add_argument('--path_discrepancies', help='Maximum Discrepancies along any Path', default=1, type=int)
group3.add_argument('--simulations', help='Maximum Trajectories Simulated for Search', default=500000, type=int)
group3.add_argument('--init_leaf_nodes', help='Set leaf nodes to the value function of the base policy', default=0, type=int)
group3.add_argument('--rollout_horizon', help='Rollout horizon to set leaf values', default=5, type=int)
group3.add_argument('--rollout_trajectories', help='Number of rollout trajectories to set leaf values', default=10, type=int)

# The following two arguments are only for the dag_rollout andd mc_rollout options
group3.add_argument('--fflag', help='Q-value adjustment heuristic', default=0, type=int)
group3.add_argument('--cparam', help='Q-value adjustment parameter', default=0.1, type=float)

args = parser.parse_args();

if args.run_ldfsss and args.init_leaf_nodes == 1:
    if args.rollout_horizon < 1 or args.rollout_trajectories < 1:
        print("Rollout Horizon and Rollout Trajectories must be positive integers")
        sys.exit()

seed = random.randint(1,1000)
env = Env(args.problem, args.rddllib)
rollout_flag = 0

from tf_dnn import restore, execute, close
restore(env, args.dnn_ckpt_file)

if args.run_ldfsss:
    env.connect2dll(args.host, args.port, args.search_horizon, args.discrepancy_depth, args.num_root_succ, args.discount_factor, args.simulations, args.path_discrepancies, args.internal_node_actions, args.root_actions, seed, args.evaluate_nn, args.init_leaf_nodes, args.rollout_horizon, args.rollout_trajectories, execute, close)
elif args.dag_rollout:
    rollout_flag = 1
    env.connect2dllrollout(args.host, args.port, args.search_horizon, args.root_actions, seed, execute, close, rollout_flag, args.num_root_succ, args.discount_factor, args.num_succ, args.fflag, args.cparam)
elif args.mc_rollout:
    rollout_flag = 2
    env.connect2dllrollout(args.host, args.port, args.search_horizon, args.root_actions, seed, execute, close, rollout_flag, args.num_root_succ, args.discount_factor, args.num_succ, args.fflag, args.cparam)
