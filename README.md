# README #

### What is this repository for? ###

This is an implementation of

(1) Forward Search Sparse Sampling (FSSS) for Limited Discrepancy Choice Functions (LDCFs). This is a search procedure for online policy improvement discussed in the paper "The Choice Function Framework for Online Policy Improvement" (https://ojs.aaai.org/index.php/AAAI/article/view/6578)

(2) The baseline Online Policy Improvement procedures DAG_Rollout and MC_Rollout discussed in the paper "Online Policy Improvement for Probabilistic Planning: Benchmarks and Baselines"

The implementation is based on a version of PROST (https://github.com/prost-planner/prost.git). The programs src/search/fsss.h and src/search/opi.h are new. Changes in PROST code are marked with comments


Contents:

1) The rddlsim-master folder has a version of the RDDLSim library (https://github.com/ssanner/rddlsim) to evaluate policies

2) The domains folder has the RDDL domain and problems for five domains: sysadmin, game_of_life, tamarisk, skill_teaching and wildfire

3) The base_policies folder has the good and bad base policies for all 10 problems from the five domains





### How do I get set up? ###

RDDLSim:

1) Compile using the command

    ./compile

Please see install.txt for help

2) Run RDDLSim for a particular domain, for example, sysadmin, using the command

    ./run rddl.competition.Server sysadmin 2323 5 12345 0

Here 2323 is the port number, 5 is the number of episodes, and 12345 is the random seed. A 1 instead of 0 for the last argument will make RDDLSim exit after the evaluation




LD-FSSS:

1) Get the libraries listed in requirements.txt

2) Compile the C++ part of the implementation using the command

    ./compile





To evaluate a base policy:

1) Run RDDLSim

2) Set the hostname and port number of RDDLSim in LDS.py

3) Set the problem and dnn_ckpt_file options in LDS.py

4) Run command

    python3 LDS.py --evaluate_nn 1






To run LD-FSSS:

1) Run RDDLSim

2) Set the hostname and port number of RDDLSim in LDS.py

3) Set the problem and dnn_ckpt_file options in LDS.py

4) Set all applicable parameters in group3 applicable. Please see comments in LDS.py

5) Set parameter init_leaf_nodes in LDS.py to (i) 0 to initialize the leaf nodes to zeros (ii) 1 to initialize leaf nodes to the value obtained by rolling out the base policy. In this case, set the parameters rollout_horizon and rollout_trajectories in LDS.py

6) Run command

    python3 LDS.py --run_ldfsss 1





To run DAG_Rollout:

1) Run RDDLSim

2) Set the hostname and port number of RDDLSim in LDS.py

3) Set the problem and dnn_ckpt_file options in LDS.py

4) Set all applicable parameters in group3 applicable. Please see comments in LDS.py

5) Set parameter fflag in LDS.py to (i) 0 for the C-Heuristic (ii) 1 for the PC-Heuristic

6) Run command

    python3 LDS.py --dag_rollout 1





To run MC_Rollout:

1) Run RDDLSim

2) Set the hostname and port number of RDDLSim in LDS.py

3) Set the problem and dnn_ckpt_file options in LDS.py

4) Set all applicable parameters in group3 applicable. Please see comments in LDS.py

5) Set parameter fflag in LDS.py to (i) 0 for the C-Heuristic (ii) 1 for the PC-Heuristic

6) Run command

    python3 LDS.py --mc_rollout 1



### Who do I talk to? ###

Murugeswari (issakkim@oregonstate.edu)
