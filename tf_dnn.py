# Author: Murugeswari

import tensorflow as tf
from Env import Env
import numpy.ctypeslib as npct
import numpy as np


def restore(rddlenv, ckptfile):
    global sess, env
    env = rddlenv
    sess = tf.Session()
    model = tf.train.import_meta_graph(ckptfile + ".meta")
    model.restore(sess, ckptfile)


def execute(state, action):
    s = npct.as_array(state, (env.problem_spec.num_state_vars,))
    a = np.zeros(env.problem_spec.num_actions, dtype=np.float)
    with sess.as_default() as sess1:
        with (sess1.graph).as_default():
            X1 = tf.get_collection("X1")[0]
            Y1 = tf.get_collection("Y1")[0]
            YHAT = tf.get_collection("YHAT")[0]
            y = sess1.run(YHAT, feed_dict={X1:np.matrix(s, np.float), Y1:np.matrix(a, np.float)})
            for i in range(env.problem_spec.num_actions):
                action[i] = y[0][i]


def close():
    print('Session closed')
    sess.close()
