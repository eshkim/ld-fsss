# Author: Murugeswari

import numpy.ctypeslib as npct
import numpy as np
import ctypes
import os


class ProblemSpec(ctypes.Structure):
    _fields_ = [('num_state_vars', ctypes.c_int), ('num_action_vars', ctypes.c_int), ('num_actions', ctypes.c_int),
        ('horizon', ctypes.c_int), ('initial_state', ctypes.POINTER(ctypes.c_double))]

    def __init__(self):
        self.num_state_vars = self.num_action_vars = self.num_actions = self.horizon = 0
        self.initial_state = None

    def __repr__(self) :
        return '\nState variables     {} \nAction variables    {} \nEnumerated Actions  {} \nHorizon             {} \nInitial State       {}'.format(
                self.num_state_vars, self.num_action_vars, self.num_actions, self.horizon, npct.as_array(self.initial_state, (self.num_state_vars, ))
        )


class Env():
    def __init__(self, problem_, rddllib_):
        self.problem = problem_
        self.rddllib = ctypes.CDLL(rddllib_)
        self.problem_spec = ProblemSpec()
        self.cb_execute = ctypes.CFUNCTYPE(None, ctypes.POINTER(ctypes.c_double), ctypes.POINTER(ctypes.c_double))
        self.cb_close = ctypes.CFUNCTYPE(None)

    def connect2dll(self, host_, port_, horizon_, depth_, children_, gamma_, simulations_, discrepancies_, num_actions_, num_root_actions_, seed_, evaluate_nn_, init_leaf_nodes_, rollout_horizon_, rollout_trajectories_, cb_execute_, cb_close_):
        self.rddllib.connect2dll((self.problem).encode(), host_.encode(), port_, horizon_, depth_, children_, ctypes.c_double(gamma_), simulations_, discrepancies_, num_actions_, num_root_actions_, seed_, evaluate_nn_, init_leaf_nodes_, rollout_horizon_, rollout_trajectories_, ctypes.byref(self.problem_spec), self.cb_execute(cb_execute_), self.cb_close(cb_close_))

    def connect2dllrollout(self, host_, port_, horizon_, num_root_actions_, seed_, cb_execute_, cb_close_, rollout_flag_, num_root_succ_, gamma_, num_succ_, fflag_, cparam_):
        self.rddllib.connect2dllrollout((self.problem).encode(), host_.encode(), port_, horizon_, num_root_actions_, seed_, ctypes.byref(self.problem_spec), self.cb_execute(cb_execute_), self.cb_close(cb_close_), rollout_flag_, num_root_succ_, ctypes.c_double(gamma_), num_succ_, fflag_, ctypes.c_double(cparam_))
